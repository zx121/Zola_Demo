# Week1 Zola_Demo

## Requirements
Create a static site with Zola, Links to an external site., a Rust static site generator, that will hold all of the portfolio work in this class.

- Static site generated with Zola
- Home page and portfolio project page templates
- Styled with CSS
- GitLab repo with source code

## Preparation

1. Install Zola
We could use various means to install Zola. The fastest way to do it is to get the executable from [https://github.com/getzola/zola/releases](https://github.com/getzola/zola/releases) We have to unzip it and move it in a folder reachable in our `$PATH`, for example, `~/.local/bin`

```bash
mv zola ~/.local/bin/zola
```

2. Use the Zola and a beautiful theme

If you haven't created a zola site yet, create a new zola site with the following command (assuming your site is called myblog):

```sh
zola init myblog
```

Enter the directory:

```sh
cd myblog
```

Add the serene theme:

```sh
git submodule add -b latest https://github.com/isunjn/serene.git themes/serene
```

3. Build & Deploy

Local preview:

```sh
zola serve
```

Build the site:

```sh
zola build
```

To deploy a static site, please refer to zola's [documentation about deployment](https://www.getzola.org/documentation/deployment/overview/)


# Screenshots
Home page
![Alt text](image.png)

Projects Page
![Alt text](image-1.png)

About Page
![Alt text](image-2.png)