+++
title = "Example Post"
date = 2023-08-31
description = "Example post"
[taxonomies]
categories = ["Tech"]
tags = ["serene", "theme"]
[extra]
lang = "en"
toc = true
+++

Example1! 

## Lorem

Praesentium, nisi saepe dolor unde iusto dolore nam, vero optio consequuntur repudiandae et! 

> Example1! 

Lorem ipsum dolor sit, **amet consectetur adipisicing elit**. Atque `libero` expedita laudantium cupiditate, sit explicabo sequi ipsa! Praesentium, *nisi saepe dolor unde iusto dolore nam*, vero optio `consequuntur` repudiandae et! Atque libero [expedita laudantium cupiditate](https://example.com), sit explicabo sequi ipsa!

{% note() %}
Lorem ipsum dolor sit, amet consectetur adipisicing elit. Praesentium, nisi saepe dolor unde iusto dolore nam, vero
optioconsequuntur repudiandae et! 
{% end %}

Example1! 

> Lorem ipsum dolor sit, amet consectetur adipisicing elit. 
> > Praesentium, nisi saepe dolor unde iusto dolore nam, vero optio consequuntur repudiandae et! 

Example1! 

Lorem ipsum dolor sit, amet [consectetur adipisicing elit](https://example.com). Praesentium, nisi saepe dolor unde iusto dolore nam, vero optio consequuntur repudiandae et! 

## Praesentium

Lorem ipsum dolor sit, amet [consectetur adipisicing elit](https://example.com). Praesentium, nisi saepe dolor unde iusto dolore nam, vero optio consequuntur repudiandae et! 

> Lorem ipsum dolor sit, amet consectetur adipisicing elit. 

Praesentium, nisi saepe dolor unde iusto dolore nam, vero optio consequuntur repudiandae et! 

{{ figure(src="/assets/typical-app.webp" alt="img", via="https://github.com/tyrchen/rust-training") }}

Example1! 


## Atque

Lorem ipsum:

- Lorem ipsum dolor sit, amet consectetur adipisicing elit.

- Praesentium, nisi saepe dolor unde iusto dolore nam, vero optio

- 

Example1! 

Example1!     sit explicabo sequi ipsa!   

Lorem ipsum:

1. Lorem ipsum dolor sit, amet consectetur adipisicing elit.

2. Praesentium, nisi saepe dolor unde iusto dolore nam

3. 

4. Lorem ipsum dolor sit, amet consectetur adipisicing elit.

5. Praesentium, nisi saepe dolor unde iusto dolore nam

6. 

Example1!    

## Laudantium

Example1! 


```rs
fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    }
}

let five = Some(5);
let six = plus_one(five);
let none = plus_one(None);
```

Example1!    


## Cupiditate

Example1! 


